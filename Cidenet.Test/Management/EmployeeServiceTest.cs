﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Cidenet.Domain.Entities.Management.Employee;
using Cidenet.Domain.Entities.Parameters.Country;
using Cidenet.Domain.Interfaces.Management.Employee;
using Cidenet.Domain.Services.Management.Employee;
using Cidenet.Domain.Services.Management.Employee.Validations;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NSubstitute;
using ValidationResult = FluentValidation.Results.ValidationResult;

namespace Cidenet.Test.Management
{
    /// <summary>
    /// Test layer service
    /// </summary>
    [TestClass]
    public class EmployeeServiceTest
    {
        

        /// <summary>
        /// Get by Id service
        /// </summary>
        [TestMethod]
        public void GetByIdTest()
        {
            Mock<IEmployeeRepository> employeeRepository = new Mock<IEmployeeRepository>();
            EmployeeService employeeService = new EmployeeService(employeeRepository.Object);
            EmployeeModel employee = new EmployeeModel();
            employee.Id = 10;
            employeeRepository.Setup(a => a.GetById((int) employee.Id)).Returns(employee);
            EmployeeModel employeeById = employeeService.GetById((int) employee.Id);

            Assert.AreEqual(employee, employeeById);
        }

        [TestMethod]
        public void GetAllTest()
        {
            Mock<IEmployeeRepository> employeeRepository = new Mock<IEmployeeRepository>();
            EmployeeService employeeService = new EmployeeService(employeeRepository.Object);
            EmployeeModel employee1 = new EmployeeModel();
            employee1.Id = 1;
            EmployeeModel employee2 = new EmployeeModel();
            employee2.Id = 2;
            EmployeeModel employee3 = new EmployeeModel();
            employee3.Id = 3;
            List<EmployeeModel> employeeList = new List<EmployeeModel>();
            employeeList.Add(employee1);
            employeeList.Add(employee2);
            employeeList.Add(employee3);
            IEnumerable<EmployeeModel> list = employeeList;

            employeeRepository.Setup(a => a.GetEmployeesWithRelationships(1, 10)).Returns(list);
            var allEmployees = employeeService.GetAll(1, 10);
            Assert.AreEqual(list, allEmployees);
        }
    }
}
