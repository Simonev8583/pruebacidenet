﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cidenet.Web.Models.General;
using Cidenet.Web.Models.Parameters.Country;
using Newtonsoft.Json;

namespace Cidenet.Web.Services.Parameters
{
    /// <summary>
    /// Country service
    /// </summary>
    public static class CountryService
    {
        private static string Baseurl = "http://localhost:26040/";
        /// <summary>
        /// Get all 
        /// </summary>
        /// <returns></returns>
        public static async Task<Response<List<Country>>> GetAll()
        {
            string path = $"Country/GetAll";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync(path);
                if (Res.IsSuccessStatusCode)
                {
                    var response = Res.Content.ReadAsStringAsync().Result;
                    Response<List<Country>> employeeResponse =
                        JsonConvert.DeserializeObject<Response<List<Country>>>(response);
                    return employeeResponse;
                }

                return new Response<List<Country>>();

            }
        }
    }
}