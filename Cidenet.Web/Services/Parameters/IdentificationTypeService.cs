﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cidenet.Web.Models.General;
using Cidenet.Web.Models.Parameters.IdentificationType;
using Newtonsoft.Json;

namespace Cidenet.Web.Services.Parameters
{
    /// <summary>
    /// Type of identification
    /// </summary>
    public static class IdentificationTypeService
    {
        private static string Baseurl = "http://localhost:26040/";
        /// <summary>
        /// Get all 
        /// </summary>
        /// <returns></returns>
        public static async Task<Response<List<IdentificationType>>> GetAll()
        {
            string path = $"IdentificationType/GetAll";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync(path);
                if (Res.IsSuccessStatusCode)
                {
                    var response = Res.Content.ReadAsStringAsync().Result;
                    Response<List<IdentificationType>> employeeResponse =
                        JsonConvert.DeserializeObject<Response<List<IdentificationType>>>(response);
                    return employeeResponse;
                }

                return new Response<List<IdentificationType>>();

            }
        }
    }
}