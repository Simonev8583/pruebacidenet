﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cidenet.Web.Models.General;
using Cidenet.Web.Models.Parameters.Area;
using Newtonsoft.Json;

namespace Cidenet.Web.Services.Parameters
{
    /// <summary>
    /// Service Area
    /// </summary>
    public static class AreaService
    {
        private static string Baseurl = "http://localhost:26040/";
        /// <summary>
        /// Get all 
        /// </summary>
        /// <returns></returns>
        public static async Task<Response<List<Area>>> GetAll()
        {
            string path = $"Area/GetAll";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync(path);
                if (Res.IsSuccessStatusCode)
                {
                    var response = Res.Content.ReadAsStringAsync().Result;
                    Response<List<Area>> employeeResponse =
                        JsonConvert.DeserializeObject<Response<List<Area>>>(response);
                    return employeeResponse;
                }

                return new Response<List<Area>>();

            }
        }
    }
}