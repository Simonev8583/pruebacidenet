﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Cidenet.Web.Models.General;
using Cidenet.Web.Models.Management.Employee;
using Newtonsoft.Json;

namespace Cidenet.Web.Services.Management
{
    /// <summary>
    /// Services to consume api
    /// </summary>
    public static class EmployeeService
    {
        private static string Baseurl = "http://localhost:26040/";
        /// <summary>
        /// Get all employees from API
        /// </summary>
        /// <param name="page"></param>
        /// <param name="itemsPerpage"></param>
        /// <returns></returns>
        public static async Task<Response<List<Employee>>> GetAllEmployees(int page, int itemsPerpage)
        {
            string path = $"Employee/GetAll?page={page}&itemsPerPage={itemsPerpage}";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync(path);
                if (Res.IsSuccessStatusCode)
                {
                    var response = Res.Content.ReadAsStringAsync().Result;
                    Response<List<Employee>> employeeResponse =
                        JsonConvert.DeserializeObject<Response<List<Employee>>>(response);
                    return employeeResponse;
                }

                return new Response<List<Employee>>();

            }
        }

        /// <summary>
        /// Add employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public static async Task<Response<Employee>> AddEmployee(Employee employee)
        {
            string path = $"Employee/AddEmployee";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var myContent = JsonConvert.SerializeObject(employee);
                var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage Res = await client.PostAsync(path, byteContent);
                if (Res.IsSuccessStatusCode)
                {
                    var response = Res.Content.ReadAsStringAsync().Result;
                    Response<Employee> employeeResponse =
                        JsonConvert.DeserializeObject<Response<Employee>>(response);
                    return employeeResponse;
                }

                return new Response<Employee>();
            }
        }

        public static async Task<Response<Employee>> Delete(int id)
        {
            string path = $"Employee/Delete?id={id}";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.DeleteAsync(path);
                if (Res.IsSuccessStatusCode)
                {
                    var response = Res.Content.ReadAsStringAsync().Result;
                    Response<Employee> employeeResponse =
                        JsonConvert.DeserializeObject<Response<Employee>>(response);
                    return employeeResponse;
                }

                return new Response<Employee>();

            }
        }

        public static async Task<Response<Employee>> GetById(int id)
        {
            string path = $"Employee/GetById?id={id}";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage Res = await client.GetAsync(path);
                if (Res.IsSuccessStatusCode)
                {
                    var response = Res.Content.ReadAsStringAsync().Result;
                    Response<Employee> employeeResponse =
                        JsonConvert.DeserializeObject<Response<Employee>>(response);
                    return employeeResponse;
                }

                return new Response<Employee>();

            }
        }

        public static async Task<Response<Employee>> UpdateEmployee(Employee employee)
        {
            string path = $"Employee/Update";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var myContent = JsonConvert.SerializeObject(employee);
                var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage Res = await client.PutAsync(path, byteContent);
                if (Res.IsSuccessStatusCode)
                {
                    var response = Res.Content.ReadAsStringAsync().Result;
                    Response<Employee> employeeResponse =
                        JsonConvert.DeserializeObject<Response<Employee>>(response);
                    return employeeResponse;
                }

                return new Response<Employee>();

            }
        }
    }
}