
namespace Cidenet.Web.Models.Parameters.Area
{

    /// <summary>
    /// Area
    /// </summary>
    public class Area
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}