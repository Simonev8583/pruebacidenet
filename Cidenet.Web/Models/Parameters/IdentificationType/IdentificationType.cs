namespace Cidenet.Web.Models.Parameters.IdentificationType
{

    /// <summary>
    /// Area
    /// </summary>
    public class IdentificationType
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}