namespace Cidenet.Web.Models.Parameters.Country
{

    /// <summary>
    /// Area
    /// </summary>
    public class Country
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Domain { get; set; }
    }
}