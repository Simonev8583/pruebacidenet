﻿using System.Threading.Tasks;

namespace Cidenet.Web.Models.General
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class Response<TEntity> 
    {
        /// <summary>
        /// Generic Entity
        /// </summary>
        public TEntity Entity { get; set; }
        /// <summary>
        /// Response it's ok
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// Type of response
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }
    }
}