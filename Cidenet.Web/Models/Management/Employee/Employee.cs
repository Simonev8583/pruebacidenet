using Cidenet.Web.Models.Parameters.Area;
using Cidenet.Web.Models.Parameters.Country;
using Cidenet.Web.Models.Parameters.IdentificationType;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cidenet.Web.Models.Management.Employee
{
    /// <summary>
    /// Class employee
    /// </summary >
    public class Employee
    {
        /// <summary>
        /// Id, key
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Property first name
        /// </summary>
        [Required(ErrorMessage = "Campo obligatorio")]
        [StringLength(20, ErrorMessage = "M�ximo 20 caracteres")]
        public string FirstName { get; set; }

        /// <summary>
        /// Property second name
        /// </summary>
        public string SecondName { get; set; }

        /// <summary>
        /// Property surname
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Second surname
        /// </summary>
        public string SecondSurname { get; set; }


        /// <summary>
        /// Type of identification
        /// </summary>
        public int IdentificationTypeId { get; set; }

        /// <summary>
        /// Identification code or number
        /// </summary>
        public string Identification { get; set; }

        /// <summary>
        /// Email of employee
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Date of admission to enterprise
        /// </summary>
        public DateTime AdmissionDate { get; set; }

        /// <summary>
        /// Version string of AdmissionDate
        /// </summary>
        public string AdmissionDateToDisplay { get; set; }

        /// <summary>
        /// Id of Area
        /// </summary>
        public int AreaId { get; set; }

        /// <summary>
        /// State of employee, default : true
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// Date of register on platform
        /// </summary>
        public DateTime RegistrationDate { get; set; }

        /// <summary>
        /// Version string of RegistrationDate
        /// </summary>
        public string RegistrationDateToDisplay { get; set; }

        /// <summary>
        /// Id of country to enable the employee
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Country object
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Area object
        /// </summary>
        public Area Area { get; set; }

        /// <summary>
        /// Identification type Object
        /// </summary>
        public IdentificationType IdentificationType { get; set; }
    }

}