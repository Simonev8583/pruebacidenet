﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Cidenet.Web.Models.General;
using Cidenet.Web.Models.Management.Employee;
using Cidenet.Web.Models.Parameters.Area;
using Cidenet.Web.Models.Parameters.Country;
using Cidenet.Web.Models.Parameters.IdentificationType;
using Cidenet.Web.Services.Management;
using Cidenet.Web.Services.Parameters;
using PagedList;

namespace Cidenet.Web.Controllers
{
    /// <summary>
    /// Controller for Employees and main screen
    /// </summary>
    public class HomeController : Controller
    {
        
        /// <summary>
        /// Index
        /// </summary>
        /// <param name="page"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(int page = 1, string search = "") 
        { 
            var response = await EmployeeService.GetAllEmployees(1, 1000);
            List<Employee> employees = response.Entity;
            employees.ForEach(x =>
            {
                x.RegistrationDateToDisplay = x.RegistrationDate.ToString("dd/MM/yyyy HH:mm:ss");
                x.AdmissionDateToDisplay = x.AdmissionDate.ToString("dd/MM/yyyy");

            });
            ViewData["CurrentFilter"] = search;

            if (!String.IsNullOrEmpty(search))
            {
                page = 1;
                employees = employees.Where(s => 
                        s.FirstName.ToLower().Contains(search.ToLower()) ||
                        s.SecondName.ToLower().Contains(search.ToLower()) ||
                        s.Surname.ToLower().Contains(search.ToLower()) ||
                        s.SecondSurname.ToLower().Contains(search.ToLower()) ||
                        s.IdentificationType.Name.ToLower().Contains(search.ToLower()) ||
                        s.Identification.ToLower().Contains(search.ToLower())).ToList();
            }
            int pageSize = 10;
            PagedList<Employee> model = new PagedList<Employee>(employees, page, pageSize);
            return View(model);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> Edit(int id)
        {
            ViewBag.Areas = await LoadAreas();
            ViewBag.Countries = await LoadCountries();
            ViewBag.IdentificationTypes = await LoadIdentificationTypes();

            var response = await EmployeeService.GetById((int)id);
            Employee employee = response.Entity;
            employee.RegistrationDateToDisplay = employee.RegistrationDate.ToString("dd/MM/yyyy HH:mm:ss");
            employee.AdmissionDateToDisplay = employee.AdmissionDate.ToString("dd/MM/yyyy");
            return View(employee);
        }

        /// <summary>
        /// Confirmed delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteConfirmed(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var response = await EmployeeService.Delete((int)id);
            if (response.Success)
            {
                ViewBag.Message = "Se agregó un nuevo empleado al sistema";
                return RedirectToAction("Index");
            }
            ViewBag.Message = response.Message;
            return View();
        }

        /// <summary>
        /// Load screen to add new employee
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Create()
        {
            ViewBag.Areas = await LoadAreas();
            ViewBag.Countries = await LoadCountries();
            ViewBag.IdentificationTypes = await LoadIdentificationTypes();
            Employee employee = new Employee();
            return View(employee);
        }

        /// <summary>
        /// Event of update 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public async Task<ActionResult> OnUpdate(Employee employee)
        {
            var response = await EmployeeService.UpdateEmployee(employee);
            ViewBag.Areas = await LoadAreas();
            ViewBag.Countries = await LoadCountries();
            ViewBag.IdentificationTypes = await LoadIdentificationTypes();
            if (response.Success)
            {
                ViewBag.Message = "Se actualizó la información del empleado";
                return RedirectToAction("Index");
            }

            ViewBag.Message = response.Message;
            return View("Edit", employee);
        }

        /// <summary>
        /// Agregar empleado
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public async Task<ActionResult> Submit(Employee employee)
        {
            var response = await EmployeeService.AddEmployee(employee);
            ViewBag.Areas = await LoadAreas();
            ViewBag.Countries = await LoadCountries();
            ViewBag.IdentificationTypes = await LoadIdentificationTypes();
            if (response.Success)
            {
                ViewBag.Message = "Se agregó un nuevo empleado al sistema";
                return RedirectToAction("Index");
            }

            ViewBag.Message = response.Message;
            return View("Create", employee); 
        }


        private async Task<List<SelectListItem>> LoadCountries()
        {
            var responseCountries = await CountryService.GetAll();
            List<Country> countries = responseCountries.Entity;
            List<SelectListItem> countryList = new List<SelectListItem>();
            countries.ForEach(x =>
            {
                countryList.Add(new SelectListItem() { Text = x.Name, Value = x.Id.ToString() });
            });
            return countryList;
        }

        private async Task<List<SelectListItem>> LoadAreas()
        {
            var responseAreas = await AreaService.GetAll();
            List<Area> areas = responseAreas.Entity;

            List<SelectListItem> areaList = new List<SelectListItem>();
            areas.ForEach(x =>
            {
                areaList.Add(new SelectListItem() { Text = x.Name, Value = x.Id.ToString() });
            });
            return areaList;
        }

        private async Task<List<SelectListItem>> LoadIdentificationTypes()
        {
            var responseTypes = await IdentificationTypeService.GetAll();
            List<IdentificationType> identificationTypes = responseTypes.Entity;

            List<SelectListItem> identificationList = new List<SelectListItem>();
            identificationTypes.ForEach(x =>
            {
                identificationList.Add(new SelectListItem() { Text = x.Name, Value = x.Id.ToString() });
            });
            return identificationList;
        }
    
    }
}