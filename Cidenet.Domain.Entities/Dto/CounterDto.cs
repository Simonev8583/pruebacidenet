﻿
namespace Cidenet.Domain.Entities.Dto
{
    /// <summary>
    /// Counter DTO
    /// </summary>
    public class CounterDto
    {
        /// <summary>
        /// Key
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Value counter
        /// </summary>
        public int Counter { get; set; }
    }
}
