﻿using Dapper.Contrib.Extensions;

namespace Cidenet.Domain.Entities.Parameters.IdentificationType
{
    /// <summary>
    /// Identification types class
    /// </summary>
    [Table("dbo.IdentificationTypes")]
    public class IdentificationTypeModel
    {
        /// <summary>
        /// Key
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Name of type
        /// </summary>
        public string Name { get; set; }
    }
}
