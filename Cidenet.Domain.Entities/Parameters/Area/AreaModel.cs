﻿using Dapper.Contrib.Extensions;

namespace Cidenet.Domain.Entities.Parameters.Area
{
    /// <summary>
    /// Class Area
    /// </summary>
    [Table("dbo.Areas")]
    public class AreaModel
    {
        /// <summary>
        /// Key
        /// </summary>
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
