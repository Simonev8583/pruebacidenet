﻿using System;
using Dapper.Contrib.Extensions;

namespace Cidenet.Domain.Entities.Parameters.Country
{
    /// <summary>
    /// Class employee
    /// </summary>
    [Table("dbo.Countries")]
    public class CountryModel
    {
        /// <summary>
        /// Id, key
        /// </summary>
        [Key]
        public virtual long Id { get; set; }
        /// <summary>
        /// Property  name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Property domain defined in the country
        /// </summary>
        public string Domain { get; set; }
    }
}
