﻿using System;
using Cidenet.Domain.Entities.Parameters.Area;
using Cidenet.Domain.Entities.Parameters.Country;
using Cidenet.Domain.Entities.Parameters.IdentificationType;
using Dapper.Contrib.Extensions;

namespace Cidenet.Domain.Entities.Management.Employee
{
    /// <summary>
    /// Class employee
    /// </summary>
    [Table("dbo.Employees")]
    public class EmployeeModel
    {
        /// <summary>
        /// Id, key
        /// </summary>
        [Key]
        public virtual long Id { get; set; }
        /// <summary>
        /// Property first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Property second name
        /// </summary>
        public string SecondName { get; set; }

        /// <summary>
        /// Property surname
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Second surname
        /// </summary>
        public string SecondSurname { get; set; }


        /// <summary>
        /// Type of identification
        /// </summary>
        public int IdentificationTypeId { get; set; }

        /// <summary>
        /// Identification code or number
        /// </summary>
        public string Identification { get; set; }

        /// <summary>
        /// Email of employee
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Date of admission to enterprise
        /// </summary>
        public DateTime AdmissionDate { get; set; }

        /// <summary>
        /// Id of Area
        /// </summary>
        public int AreaId { get; set; }

        /// <summary>
        /// State of employee, default : true
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// Date of register on platform
        /// </summary>
        public DateTime RegistrationDate { get; set; }

        /// <summary>
        /// Id of country to enable the employee
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Date of edition
        /// </summary>
        public DateTime? EditionDate { get; set; }

        /// <summary>
        /// Country object
        /// </summary>
        [Computed]
        public CountryModel Country { get; set; }

        /// <summary>
        /// Area object
        /// </summary>
        [Computed]
        public AreaModel Area { get; set; }

        /// <summary>
        /// Identification type Object
        /// </summary>
        [Computed]
        public IdentificationTypeModel IdentificationType { get; set; }
    }
}
