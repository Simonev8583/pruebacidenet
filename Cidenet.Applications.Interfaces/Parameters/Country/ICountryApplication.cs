﻿using System;
using System.Collections.Generic;
using System.Text;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Domain.Entities.Parameters.Country;

namespace Cidenet.Applications.Interfaces.Parameters.Country
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface ICountryApplication
    {
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        Response<IEnumerable<CountryModel>> GetAll();
    }
}
