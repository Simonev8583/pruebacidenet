﻿using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Domain.Entities.Parameters.IdentificationType;

namespace Cidenet.Applications.Interfaces.Parameters.IdentificationType
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface IIdentificationTypeApplication
    {
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        Response<IEnumerable<IdentificationTypeModel>> GetAll();
    }
}
