﻿using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Domain.Entities.Parameters.Area;

namespace Cidenet.Applications.Interfaces.Parameters.Area
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface IAreaApplication
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Response<IEnumerable<AreaModel>> GetAll();
    }
}
