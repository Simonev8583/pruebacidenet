﻿using System;
using System.Collections.Generic;
using System.Text;
using Cidenet.Infra.Constants.Exceptions;

namespace Cidenet.Applications.Interfaces.Dtos.General
{
    public class Response<TEntity>
    {
        public Response()
        {
            Success = false;
        }

        /// <summary>
        /// 
        /// </summary>
        public TEntity Entity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ExceptionTypes Type { get; set; }
    }
}
