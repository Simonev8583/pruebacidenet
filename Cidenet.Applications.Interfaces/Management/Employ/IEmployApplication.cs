﻿
using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Domain.Entities.Management.Employee;

namespace Cidenet.Applications.Interfaces.Management.Employ
{
    /// <summary>
    /// Interface 
    /// </summary>
    public interface IEmployApplication
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        Response<EmployeeModel> AddEmployee(EmployeeModel employee);

        /// <summary>
        /// Get all employees with relationships
        /// </summary>
        /// <returns></returns>
        Response<IEnumerable<EmployeeModel>> GetAllEmployees(int page, int itemsPerPage);

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Response<EmployeeModel> GetEmployeeById(int id);

        /// <summary>
        /// Delete an employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Response<EmployeeModel> Delete(long id);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        Response<EmployeeModel> Update(EmployeeModel employee);
    }
}
