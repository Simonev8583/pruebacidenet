﻿using System;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Infra.Constants.Exceptions;

namespace Cidenet.Applications.Services.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationGeneric
    {
        /// <summary>
        /// Try exception
        /// </summary>
        /// <param name="action"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Response<T> Try<T>(Func<T> action)
        {
            var response = new Response<T>();
            try
            {
                response.Entity = action();
                response.Success = true;
                response.Type = ExceptionTypes.Success;
            }
            catch (ManagerException ex)
            {
                response.Message = ex.Message;
                response.Type = ex.Type;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }

            return response;
        }
    }
}
