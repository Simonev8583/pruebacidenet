﻿using System.Collections.Generic;
using System.Text;
using System.Transactions;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Applications.Interfaces.Management.Employ;
using Cidenet.Applications.Services.Helpers;
using Cidenet.Domain.Entities.Management.Employee;
using Cidenet.Domain.Interfaces.Management.Employee;

namespace Cidenet.Applications.Services.Management.Employ
{
    /// <summary>
    /// Application layer
    /// </summary>
    public class EmployApplication : IEmployApplication
    {
        private readonly IEmployeeService _employService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="employService"></param>
        public EmployApplication(IEmployeeService employService)
        {
            _employService = employService;
        }

        /// <summary>
        /// Add employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public Response<EmployeeModel> AddEmployee(EmployeeModel employee)
        {
            return ApplicationGeneric.Try(() =>
            {
                using (var scope = new TransactionScope())
                {
                    _employService.AddEmployee(employee);
                    scope.Complete();
                    return employee;
                }
            });
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        public Response<IEnumerable<EmployeeModel>> GetAllEmployees(int page, int itemsPerPage)
        {
            return ApplicationGeneric.Try(() => _employService.GetAll(page, itemsPerPage));
        }

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<EmployeeModel> GetEmployeeById(int id)
        {
            return ApplicationGeneric.Try(() => _employService.GetById(id));
        }

        /// <summary>
        /// Delete 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response<EmployeeModel> Delete(long id)
        {
            return ApplicationGeneric.Try<EmployeeModel>(() =>
            {
                using (var scope = new TransactionScope())
                {
                    _employService.Delete(id);
                    scope.Complete();
                    return null;
                }
            });
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public Response<EmployeeModel> Update(EmployeeModel employee)
        {
            return ApplicationGeneric.Try(() =>
            {
                using (var scope = new TransactionScope())
                {
                    EmployeeModel oldEmployee = _employService.GetById((int)employee.Id);
                    employee.RegistrationDate = oldEmployee.RegistrationDate;
                    employee.Email = oldEmployee.Email;
                    _employService.Update(employee);
                    scope.Complete();
                    return employee;
                }
            });
        }
    }
}
