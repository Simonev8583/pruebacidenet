﻿using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Applications.Interfaces.Parameters.Area;
using Cidenet.Applications.Services.Helpers;
using Cidenet.Domain.Entities.Parameters.Area;
using Cidenet.Domain.Interfaces.Parameters.Area;

namespace Cidenet.Applications.Services.Parameters.Area
{
    /// <summary>
    /// Implement
    /// </summary>
    public class AreaApplication: IAreaApplication
    {
        private readonly IAreaService _areaService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="areaService"></param>
        public AreaApplication(IAreaService areaService)
        {
            _areaService = areaService;
        }
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public Response<IEnumerable<AreaModel>> GetAll()
        {
            return ApplicationGeneric.Try(() => _areaService.GetAll());
        }
    }
}
