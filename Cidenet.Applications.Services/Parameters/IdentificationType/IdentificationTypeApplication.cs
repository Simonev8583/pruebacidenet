﻿using System;
using System.Collections.Generic;
using System.Text;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Applications.Interfaces.Parameters.IdentificationType;
using Cidenet.Applications.Services.Helpers;
using Cidenet.Domain.Entities.Parameters.IdentificationType;
using Cidenet.Domain.Interfaces.Parameters.IdentificationType;

namespace Cidenet.Applications.Services.Parameters.IdentificationType
{
    /// <summary>
    /// Implement
    /// </summary>
    public class IdentificationTypeApplication: IIdentificationTypeApplication
    {
        private readonly IIdentificationTypeService _identificationTypeService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="identificationTypeService"></param>
        public IdentificationTypeApplication(IIdentificationTypeService identificationTypeService)
        {
            _identificationTypeService = identificationTypeService;
        }
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public Response<IEnumerable<IdentificationTypeModel>> GetAll()
        {
            return ApplicationGeneric.Try(() => _identificationTypeService.GetAll());
        }
    }
}
