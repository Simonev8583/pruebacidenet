﻿using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Applications.Interfaces.Parameters.Country;
using Cidenet.Applications.Services.Helpers;
using Cidenet.Domain.Entities.Parameters.Country;
using Cidenet.Domain.Interfaces.Parameters.Country;

namespace Cidenet.Applications.Services.Parameters.Country
{
    /// <summary>
    /// Implement
    /// </summary>
    public class CountryApplication: ICountryApplication
    {
        private readonly ICountryService _countryService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="countryService"></param>
        public CountryApplication(ICountryService countryService)
        {
            _countryService = countryService;
        }


        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public Response<IEnumerable<CountryModel>> GetAll()
        {
            return ApplicationGeneric.Try(() => _countryService.GetAll());
        }
    }
}
