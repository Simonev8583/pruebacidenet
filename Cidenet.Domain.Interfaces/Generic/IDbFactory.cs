﻿using System.Data;

namespace Cidenet.Domain.Interfaces.Generic
{
    /// <summary>
    /// Interface DB Factory
    /// </summary>
    public interface IDbFactory
    {
        /// <summary>
        /// Get current connection to DB
        /// </summary>
        /// <returns></returns>
        IDbConnection GetConnection();
    }
}
