﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Management.Employee;
using Cidenet.Domain.Entities.Parameters.Country;

namespace Cidenet.Domain.Interfaces.Management.Employee
{
    /// <summary>
    /// Interface to Employee repository
    /// </summary>
    public interface IEmployeeRepository
    {
        /// <summary>
        /// Add new employ in DB
        /// </summary>
        long AddEmployee(EmployeeModel employee);

        /// <summary>
        /// Validation
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        bool NoExistIdentification(EmployeeModel employee);

        /// <summary>
        /// Validate Id
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        bool NoExistIdentification(EmployeeModel employee, long id);

        /// <summary>
        /// Get country by id country of employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        CountryModel GetCountryById(int id);

        /// <summary>
        /// Search if exist the email generated
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        bool NoExistEmail(string email);

        /// <summary>
        /// Validate if exist an area by Id
        /// </summary>
        /// <param name="idArea"></param>
        /// <returns></returns>
        bool ExistArea(int idArea);

        /// <summary>
        /// Validate if exist a type of identification by id
        /// </summary>
        /// <param name="idType"></param>
        /// <returns></returns>
        bool ExistIdentificationType(int idType);

        /// <summary>
        /// Get all with relationships 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="itemsPerPage"></param>
        /// <returns></returns>
        IEnumerable<EmployeeModel> GetEmployeesWithRelationships(int page, int itemsPerPage);

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        EmployeeModel GetById(int id);

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="employee"></param>
        void Delete(EmployeeModel employee);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="employee"></param>
        void UpdateEmployee(EmployeeModel employee);
    }
}
