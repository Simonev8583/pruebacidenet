﻿

using System.Collections;
using System.Collections.Generic;
using System.Text;
using Cidenet.Domain.Entities.Management.Employee;

namespace Cidenet.Domain.Interfaces.Management.Employee
{
    /// <summary>
    /// Interface employee service
    /// </summary>
    public interface IEmployeeService
    {
        /// <summary>
        /// Validate data employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        long AddEmployee(EmployeeModel employee);

        /// <summary>
        /// Get all employees
        /// </summary>
        /// <param name="page"></param>
        /// <param name="itemsPerPage"></param>
        /// <returns></returns>
        IEnumerable<EmployeeModel> GetAll(int page, int itemsPerPage);

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        EmployeeModel GetById(int id);

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        void Delete(long id);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="employee"></param>
        void Update(EmployeeModel employee);
    }
}
