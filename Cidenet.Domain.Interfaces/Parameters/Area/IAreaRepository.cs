﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.Area;

namespace Cidenet.Domain.Interfaces.Parameters.Area
{
    /// <summary>
    /// Intergace
    /// </summary>
    public interface IAreaRepository
    {
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        IEnumerable<AreaModel> GetAll();
    }
}
