﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.Area;

namespace Cidenet.Domain.Interfaces.Parameters.Area
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface IAreaService
    {
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        IEnumerable<AreaModel> GetAll();
    }
}
