﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.Country;

namespace Cidenet.Domain.Interfaces.Parameters.Country
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface ICountryService
    {
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        IEnumerable<CountryModel> GetAll();
    }
}
