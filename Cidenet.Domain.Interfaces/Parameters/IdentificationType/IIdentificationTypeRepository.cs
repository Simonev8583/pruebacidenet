﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.IdentificationType;

namespace Cidenet.Domain.Interfaces.Parameters.IdentificationType
{
    /// <summary>
    /// Interface
    /// </summary>
    public interface IIdentificationTypeRepository
    {
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        IEnumerable<IdentificationTypeModel> GetAll();
    }
}
