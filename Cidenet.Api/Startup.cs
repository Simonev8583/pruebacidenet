using Cidenet.Infra.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Cidenet.Api
{
    public class Startup
    {
        private ApplicationInstaller applicationInstaller;
        private ServiceInstaller serviceInstaller;
        private RepositoryInstaller repositoryInstaller;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            applicationInstaller = new ApplicationInstaller(services);
            serviceInstaller = new ServiceInstaller(services);
            repositoryInstaller = new RepositoryInstaller(services);

            services.AddCors(c =>
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin()));

            services.AddControllers();
            AddSwagger(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(options => options.AllowAnyOrigin());
            var services = app.ApplicationServices;
            applicationInstaller.Install(services);
            serviceInstaller.Install(services);
            repositoryInstaller.Install(services);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test Cidenet API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo()
                {
                    Title = $"Test Cidenet {groupName}",
                    Version = groupName,
                    Description = "Test Cidenet API",
                    Contact = new OpenApiContact()
                    {
                        Name = "Daniel Simone Villa G�mez",
                        Email = "simone.villa18583@hotmail.com"
                    }
                });
            });
        }
    }
}
