using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Applications.Interfaces.Parameters.Area;
using Cidenet.Domain.Entities.Parameters.Area;
using Microsoft.AspNetCore.Mvc;

namespace Cidenet.Api.Controllers.Parameters.Area
{
    /// <summary>
    /// controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class AreaController : ControllerBase
    {
        private readonly IAreaApplication _areaApplication;
        /// <summary>
        /// Constructor
        /// </summary>
        public AreaController(IAreaApplication areaApplication)
        {
            _areaApplication = areaApplication;
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAll")]
        public Response<IEnumerable<AreaModel>> GetAll()
        {
            return _areaApplication.GetAll();
        }

    }
}