using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Applications.Interfaces.Parameters.IdentificationType;
using Cidenet.Domain.Entities.Parameters.IdentificationType;
using Microsoft.AspNetCore.Mvc;

namespace Cidenet.Api.Controllers.Parameters.IdentificationType
{
    /// <summary>
    /// controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class IdentificationTypeController : ControllerBase
    {
        private readonly IIdentificationTypeApplication _identificationTypeApplication;
        /// <summary>
        /// Constructor
        /// </summary>
        public IdentificationTypeController(IIdentificationTypeApplication identificationTypeApplication)
        {
            _identificationTypeApplication = identificationTypeApplication;

        }

        /// <summary>
        /// Get all 
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAll")]
        public Response<IEnumerable<IdentificationTypeModel>> GetAll()
        {
            return _identificationTypeApplication.GetAll();
        }

    }
}
