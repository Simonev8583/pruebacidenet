using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Applications.Interfaces.Parameters.Country;
using Cidenet.Domain.Entities.Parameters.Country;
using Microsoft.AspNetCore.Mvc;

namespace Cidenet.Api.Controllers.Parameters.Country
{
    /// <summary>
    /// controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class CountryController : ControllerBase
    {
        private readonly ICountryApplication _countryApplication;
        /// <summary>
        /// Constructor
        /// </summary>
        public CountryController(ICountryApplication countryApplication)
        {
            _countryApplication = countryApplication;
        }

        /// <summary>
        /// Get all 
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAll")]
        public Response<IEnumerable<CountryModel>> GetAll()
        {
            return _countryApplication.GetAll();
        }

    }
}