﻿using System.Collections.Generic;
using Cidenet.Applications.Interfaces.Dtos.General;
using Cidenet.Applications.Interfaces.Management.Employ;
using Cidenet.Domain.Entities.Management.Employee;
using Microsoft.AspNetCore.Mvc;

namespace Cidenet.Api.Controllers.Management
{
    /// <summary>
    /// Employee controller
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployApplication _employApplication;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="employApplication"></param>
        public EmployeeController(IEmployApplication employApplication)
        {
            _employApplication = employApplication;

        }

        /// <summary>
        /// Add new employee
        /// </summary>
        /// <returns></returns>
        [HttpPost("AddEmployee")]
        public Response<EmployeeModel> Post(EmployeeModel employee)
        {
            return _employApplication.AddEmployee(employee);
        }

        /// <summary>
        /// Get all employees
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAll")]
        public Response<IEnumerable<EmployeeModel>> GetAll( int page,  int itemsPerPage)
        {
            return _employApplication.GetAllEmployees(page, itemsPerPage);
        }

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetById")]
        public Response<EmployeeModel> GetById(int id)
        {
            return _employApplication.GetEmployeeById(id);
        }

        /// <summary>
        /// Delete an employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("Delete")]
        public Response<EmployeeModel> Delete(long id)
        {
            return _employApplication.Delete(id);
        }

        /// <summary>
        /// Update an employee
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("Update")]
        public Response<EmployeeModel> Update(EmployeeModel employee)
        {
            return _employApplication.Update(employee);
        }
    }
}
