﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cidenet.Infra.Constants.Exceptions
{
    /// <summary>
    /// Manager the exceptions
    /// </summary>
    public class ManagerException : Exception
    {
        public ExceptionTypes Type { get; }
        /// <summary>
        /// Constructor to General
        /// </summary>
        public ManagerException()
        {
            Type = ExceptionTypes.Generic;
        }

        /// <summary>
        /// Generic with message
        /// </summary>
        /// <param name="message"></param>
        public ManagerException(string message) : base(message)
        {
            Type = ExceptionTypes.Generic;
        }

        /// <summary>
        /// With message and type of exception
        /// </summary>
        /// <param name="exceptionType"></param>
        /// <param name="message"></param>
        public ManagerException(ExceptionTypes exceptionType, string message) : base(message.Replace("Validation failed", "Validación fallida"))
        {
            Type = exceptionType;
        }

        /// <summary>
        /// With list of exceptions
        /// </summary>
        /// <param name="exceptionType"></param>
        /// <param name="messages"></param>
        public ManagerException(ExceptionTypes exceptionType, List<string> messages) : base(string.Join("\r\n", messages.Select(r => $"*{r}")))
        {
            Type = exceptionType;
        }

        /// <summary>
        /// Inner exception
        /// </summary>
        /// <param name="exceptionType"></param>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public ManagerException(ExceptionTypes exceptionType, string message, Exception innerException) : base(message,
            innerException)
        {
            Type = exceptionType;
        }
    }
}
