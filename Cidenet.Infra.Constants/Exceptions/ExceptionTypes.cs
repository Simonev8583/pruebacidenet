﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cidenet.Infra.Constants.Exceptions
{
    /// <summary>
    /// The gestion exception types
    /// </summary>
    public enum ExceptionTypes
    {
        /// <summary>
        /// General exceptions
        /// </summary>
        Generic,
        /// <summary>
        /// Databases errors
        /// </summary>
        DataBase,
        /// <summary>
        /// Validations
        /// </summary>
        Validations,
        /// <summary>
        /// Auth
        /// </summary>
        Authentication,
        /// <summary>
        /// Config
        /// </summary>
        Configuration,
        /// <summary>
        /// WebService
        /// </summary>
        WebService,
        /// <summary>
        /// Response OK
        /// </summary>
        Success
    }
}
