﻿using Cidenet.Domain.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using Cidenet.Domain.Interfaces.Generic;
using Cidenet.Domain.Interfaces.Management.Employee;
using Cidenet.Domain.Interfaces.Parameters.Area;
using Cidenet.Domain.Interfaces.Parameters.Country;
using Cidenet.Domain.Interfaces.Parameters.IdentificationType;
using Cidenet.Infra.Data.Repository.Generic;
using Cidenet.Infra.Data.Repository.Management.Employee;
using Cidenet.Infra.Data.Repository.Parameters.Area;
using Cidenet.Infra.Data.Repository.Parameters.Country;
using Cidenet.Infra.Data.Repository.Parameters.IdentificationType;
using Microsoft.Extensions.Configuration;

//using Microsoft.Extensions.Configuration.Json;
//using Microsoft.Extensions.Configuration.FileExtensions;

namespace Cidenet.Infra.IoC
{
    /// <summary>
    /// Container for repositories
    /// </summary>
    public class RepositoryInstaller
    {
        /// <summary>
        /// Constructor container
        /// </summary>
        /// <param name="services"></param>
        public RepositoryInstaller(IServiceCollection services)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true).Build();
            var stringConnection = configuration.GetSection("ConnectionStrings");

            services.AddTransient<IDbFactory>(s => new DbFactory(stringConnection.Value));
            
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
            services.AddTransient<IAreaRepository, AreaRepository>();
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<IIdentificationTypeRepository, IdentificationTypeRepository>();

        }


        /// <summary>
        /// Build the new instances
        /// </summary>
        /// <param name="services"></param>
        public void Install(IServiceProvider services)
        {
            services.GetService<IDbFactory>();
            services.GetService<IEmployeeRepository>();
            services.GetService<IAreaRepository>();
            services.GetService<ICountryRepository>();
            services.GetService<IIdentificationTypeRepository>();
        }
    }
}
