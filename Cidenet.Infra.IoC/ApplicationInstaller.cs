﻿using Cidenet.Applications.Interfaces.Management.Employ;
using Cidenet.Applications.Services.Management.Employ;
using Microsoft.Extensions.DependencyInjection;
using System;
using Cidenet.Applications.Interfaces.Parameters.Area;
using Cidenet.Applications.Interfaces.Parameters.Country;
using Cidenet.Applications.Interfaces.Parameters.IdentificationType;
using Cidenet.Applications.Services.Parameters.Area;
using Cidenet.Applications.Services.Parameters.Country;
using Cidenet.Applications.Services.Parameters.IdentificationType;

namespace Cidenet.Infra.IoC
{
    /// <summary>
    /// Container Applications
    /// </summary>
    public class ApplicationInstaller
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        public ApplicationInstaller(IServiceCollection services)
        {
            services.AddTransient<IEmployApplication, EmployApplication>();
            services.AddTransient<IAreaApplication, AreaApplication>();
            services.AddTransient<ICountryApplication, CountryApplication>();
            services.AddTransient<IIdentificationTypeApplication, IdentificationTypeApplication>();

        }

        public void Install(IServiceProvider services)
        {
            services.GetService<IEmployApplication>();
            services.GetService<IAreaApplication>();
            services.GetService<ICountryApplication>();
            services.GetService<IIdentificationTypeApplication>();
        }
    }
}
