﻿using Cidenet.Domain.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using Cidenet.Domain.Interfaces.Management.Employee;
using Cidenet.Domain.Interfaces.Parameters.Area;
using Cidenet.Domain.Interfaces.Parameters.Country;
using Cidenet.Domain.Interfaces.Parameters.IdentificationType;
using Cidenet.Domain.Services.Management.Employee;
using Cidenet.Domain.Services.Parameters.Area;
using Cidenet.Domain.Services.Parameters.Country;
using Cidenet.Domain.Services.Parameters.IdentificationType;

namespace Cidenet.Infra.IoC
{
    /// <summary>
    /// Container
    /// </summary>
    public class ServiceInstaller
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        public ServiceInstaller(IServiceCollection services)
        {
            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddTransient<IAreaService, AreaService>();
            services.AddTransient<ICountryService, CountryService>();
            services.AddTransient<IIdentificationTypeService, IdentificationTypeService>();

        }

        /// <summary>
        /// Build instances
        /// </summary>
        /// <param name="services"></param>
        public void Install(IServiceProvider services)
        {
            services.GetService<IEmployeeService>();
            services.GetService<IAreaService>();
            services.GetService<ICountryService>();
            services.GetService<IIdentificationTypeService>();
        }
    }
}
