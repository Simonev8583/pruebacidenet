﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Management.Employee;
using Cidenet.Domain.Interfaces.Management.Employee;
using Cidenet.Domain.Services.Management.Employee.Validations;
using Cidenet.Infra.Constants.Exceptions;

namespace Cidenet.Domain.Services.Management.Employee
{
    /// <summary>
    /// Service layer 
    /// </summary>
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="employeeRepository"></param>
        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Validate employee information
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public long AddEmployee(EmployeeModel employee)
        {
            var validator = new EmployeeValidations(_employeeRepository);
            var isValid = validator.Validate(employee);
            if (isValid.IsValid)
            {
                return _employeeRepository.AddEmployee(employee);
            }

            throw new ManagerException(ExceptionTypes.Validations, isValid.Errors[0].ErrorMessage);
        }

        /// <summary>
        /// Get all 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="itemsPerPage"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeModel> GetAll(int page, int itemsPerPage)
        {
            return _employeeRepository.GetEmployeesWithRelationships(page, itemsPerPage);
        }

        /// <summary>
        /// Get by Id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EmployeeModel GetById(int id)
        {
            return _employeeRepository.GetById(id);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        public void Delete(long id)
        {
            EmployeeModel employee = new EmployeeModel {Id = id};
            _employeeRepository.Delete(employee);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="employee"></param>
        public void Update(EmployeeModel employee)
        {
            var validator = new EmployeeValidations(_employeeRepository);
            var isValid = validator.Validate(employee);
            if (isValid.IsValid)
            {
                
                _employeeRepository.UpdateEmployee(employee);
            }
            else
            {
                throw new ManagerException(ExceptionTypes.Validations, isValid.Errors[0].ErrorMessage);
            }
        }
    }
}
