﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using Cidenet.Domain.Entities.Management.Employee;
using Cidenet.Domain.Entities.Parameters.Country;
using Cidenet.Domain.Interfaces.Management.Employee;
using FluentValidation;

namespace Cidenet.Domain.Services.Management.Employee.Validations
{
    /// <summary>
    /// Validator
    /// </summary>
    public class EmployeeValidations : AbstractValidator<EmployeeModel>
    {
        private readonly IEmployeeRepository _employeeRepository;

        /// <summary>
        /// Validator
        /// </summary>
        /// <param name="employeeRepository"></param>
        public EmployeeValidations(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
            this.RuleFor(r => r.FirstName).NotEmpty().Length(1, 20).Matches(@"^[A-Z]+$")
                .WithMessage("La longitud del campo primer nombre debe estar entre 1 y 20 caracteres en mayuscula. ");
            this.RuleFor(r => r.SecondName).Length(0, 50).Matches(@"^[A-Z\s]+$")
                .WithMessage("La longitud del campo segundo nombre debe estar entre 0 y 50 caracteres en mayuscula. ");
            this.RuleFor(r => r.Surname).NotEmpty().Length(1, 20).Matches(@"^[A-Z\s]+$")
                .WithMessage("La longitud del campo apellido debe estar entre 1 y 20 caracteres en mayuscula. ");
            this.RuleFor(r => r.SecondSurname).NotEmpty().Length(1, 20).Matches(@"^[A-Z\s]+$")
                .WithMessage("La longitud del campo segundo apellido debe estar entre 1 y 20 caracteres en mayuscula. ");
            
            this.RuleFor(r => r.Identification).NotEmpty().Length(1, 20).Matches(@"^[A-Za-z0-9]+$")
                .WithMessage("La longitud de la Identificación debe estar entre 1 y 20 caracteres alfanumericos");
            this.RuleFor(r => r).Must(ValidateEmail).WithMessage("Problema al generar el email");
            this.RuleFor(r => r.CountryId).NotEmpty().Must(ExistCountry).WithMessage("No existe un País con ese identificador");
            this.RuleFor(r => r.AreaId).NotEmpty().Must(ExistArea).WithMessage("No existe ninguna area con ese identificador");
            this.RuleFor(r => r.IdentificationTypeId).NotEmpty().Must(ExistIdentificationType)
                .WithMessage("No existe ningun tipo de indentificación con ese Id");
            this.RuleFor(r => r.AdmissionDate).Must(ValidateAdmissionDate).WithMessage(
                "La fecha de ingreso no es correcta, debe ser Igual a la fecha actual o menor a no más de un mes");
            this.RuleFor(r => r).Must(NoExistIdentification)
                    .WithMessage($"Ya existe un empleado con esta identificación");
        }

        private bool NoExistIdentification(EmployeeModel employee)
        {
            if (employee.Id > 0)
            {
                return _employeeRepository.NoExistIdentification(employee, employee.Id);
            }
            return _employeeRepository.NoExistIdentification(employee);
        }

        private bool ValidateEmail(EmployeeModel employee)
        {
            if (employee.Id > 0)
            {
                EmployeeModel oldEmployee = _employeeRepository.GetById((int) employee.Id);
                if (oldEmployee.FirstName != employee.FirstName || oldEmployee.Surname != employee.Surname || oldEmployee.CountryId != employee.CountryId)
                {
                    return BuildEmail(employee);
                }

                return true;
            }

            return BuildEmail(employee);
            
            
        }

        private bool BuildEmail(EmployeeModel employee)
        {
            CountryModel country = _employeeRepository.GetCountryById(employee.CountryId);
            employee.Email = string.Format("{0}.{1}{2}", employee.FirstName.Replace(" ", "").ToLower(), employee.Surname.Replace(" ", "").ToLower(), country.Domain);
            int counter = 0;
            while (!_employeeRepository.NoExistEmail(employee.Email))
            {
                counter++;
                string email = string.Format("{0}.{1}.{2}{3}", employee.FirstName.Replace(" ", "").ToLower(), employee.Surname.Replace(" ", "").ToLower(),
                    counter, country.Domain);
                employee.Email = email;
            }
            return true;
        }

        private bool ExistCountry(int idCountry)
        {
            CountryModel country = _employeeRepository.GetCountryById(idCountry);
            if (country.Name.Length > 0)
            {
                return true;
            }

            return false;
        }

        private bool ExistArea(int idArea)
        {
            return _employeeRepository.ExistArea(idArea);
        }

        private bool ExistIdentificationType(int idIdentificationType)
        {
            return _employeeRepository.ExistIdentificationType(idIdentificationType);
        }

        private bool ValidateAdmissionDate(DateTime date)
        {
            DateTime currentDate = DateTime.Now;
            if (currentDate.Date == date.Date)
            {
                return true;
            }

            if (date.Date <= currentDate.Date)
            {
                currentDate = currentDate.AddMonths(-1);
                if (date.Date >= currentDate.Date)
                {
                    return true;
                }
            }

            return false;
        }

    }
}
