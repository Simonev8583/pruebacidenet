﻿using System;
using System.Collections.Generic;
using System.Text;
using Cidenet.Domain.Entities.Parameters.IdentificationType;
using Cidenet.Domain.Interfaces.Parameters.IdentificationType;

namespace Cidenet.Domain.Services.Parameters.IdentificationType
{
    /// <summary>
    /// Implement
    /// </summary>
    public class IdentificationTypeService: IIdentificationTypeService
    {
        private readonly IIdentificationTypeRepository _identificationTypeRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="identificationTypeRepository"></param>
        public IdentificationTypeService(IIdentificationTypeRepository identificationTypeRepository)
        {
            _identificationTypeRepository = identificationTypeRepository;
        }
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IdentificationTypeModel> GetAll()
        {
            return _identificationTypeRepository.GetAll();
        }
    }
}
