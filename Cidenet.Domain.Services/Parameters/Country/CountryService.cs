﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.Country;
using Cidenet.Domain.Interfaces.Parameters.Country;

namespace Cidenet.Domain.Services.Parameters.Country
{
    /// <summary>
    /// Implement
    /// </summary>
    public class CountryService: ICountryService
    {
        private readonly ICountryRepository _countryRepository;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="countryRepository"></param>
        public CountryService(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CountryModel> GetAll()
        {
            return _countryRepository.GetAll();
        }
    }
}
