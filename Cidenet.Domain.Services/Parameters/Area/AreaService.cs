﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.Area;
using Cidenet.Domain.Interfaces.Parameters.Area;

namespace Cidenet.Domain.Services.Parameters.Area
{
    /// <summary>
    /// Implement
    /// </summary>
    public class AreaService: IAreaService
    {
        private readonly IAreaRepository _areaRepository;

        /// <summary>
        /// COntructor
        /// </summary>
        /// <param name="areaRepository"></param>
        public AreaService(IAreaRepository areaRepository)
        {
            _areaRepository = areaRepository;
        }
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AreaModel> GetAll()
        {
            return _areaRepository.GetAll();
        }
    }
}
