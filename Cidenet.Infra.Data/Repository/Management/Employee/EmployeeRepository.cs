﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Cidenet.Domain.Entities.Dto;
using Cidenet.Domain.Entities.Management.Employee;
using Cidenet.Domain.Entities.Parameters.Area;
using Cidenet.Domain.Entities.Parameters.Country;
using Cidenet.Domain.Entities.Parameters.IdentificationType;
using Cidenet.Domain.Interfaces.Generic;
using Cidenet.Domain.Interfaces.Management.Employee;
using Cidenet.Infra.Constants.Exceptions;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Cidenet.Infra.Data.Repository.Management.Employee
{
    /// <summary>
    /// Employee repository
    /// </summary>
    public class EmployeeRepository : IEmployeeRepository
    {

        private readonly IDbFactory _dbFactory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbFactory"></param>
        public EmployeeRepository(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        /// <summary>
        /// Add new employee in DB
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public long AddEmployee(EmployeeModel employee)
        {
            long id = 0;
            using (var db = _dbFactory.GetConnection())
            {
                employee.RegistrationDate = DateTime.Now;
                employee.State = true;
                id = db.Insert(employee);
            }

            return id;
        }

        /// <summary>
        /// Validation on Identification
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public bool NoExistIdentification(EmployeeModel employee)
        {
            const string query =
                "Select Count(*) as result from [dbo].[Employees] where Identification = @Identification";
            using (var db = _dbFactory.GetConnection())
            {
                var result = db.Query<int>(query, new {employee.Identification}).FirstOrDefault();
                return result <= 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool NoExistIdentification(EmployeeModel employee, long id)
        {
            const string query =
                "Select Id, Count(*) as result from [dbo].[Employees] where Identification = @Identification group by Id";
            using (var db = _dbFactory.GetConnection())
            {
                var result = db.Query<CounterDto>(query, new { employee.Identification }).FirstOrDefault();
                if (result != null)
                {
                    return result.Id == id;
                }

                return true;
            }
        }

        /// <summary>
        /// Get country
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public CountryModel GetCountryById(int id)
        {
            const string query = "Select Id, Name, Domain from [dbo].[Countries] where Id = @Id";
            using (var db = _dbFactory.GetConnection())
            {
                return db.Query<CountryModel>(query, new {Id = id}).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool NoExistEmail(string email)
        {
            const string query = "Select Count(*) as result from [dbo].[Employees] where Email = @Email";
            using (var db = _dbFactory.GetConnection())
            {
                var result = db.Query<int>(query, new {Email = email}).FirstOrDefault();
                return result <= 0;
            }
        }

        /// <summary>
        /// Validate if exist an Area by Id
        /// </summary>
        /// <param name="idArea"></param>
        /// <returns></returns>
        public bool ExistArea(int idArea)
        {
            const string query = "Select Count(*) as result from [dbo].[Areas] where Id = @IdArea";
            using (var db = _dbFactory.GetConnection())
            {
                var result = db.Query<int>(query, new {IdArea = idArea}).FirstOrDefault();
                return result > 0;
            }
        }

        /// <summary>
        /// Validate if exist a Identification Type 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ExistIdentificationType(int id)
        {
            const string query = "Select Count(*) as result from [dbo].[IdentificationTypes] where Id = @IdType";
            using (var db = _dbFactory.GetConnection())
            {
                var result = db.Query<int>(query, new {IdType = id}).FirstOrDefault();
                return result > 0;
            }
        }

        /// <summary>
        /// Get all with relationships
        /// </summary>
        /// <param name="page"></param>
        /// <param name="itemsPerPage"></param>
        /// <returns></returns>
        public IEnumerable<EmployeeModel> GetEmployeesWithRelationships(int page, int itemsPerPage)
        {
            //Esta consulta es preferible hacer en procedimientos almacenados SP
            if (page > 0)
            {
                page = page - 1;
            }
            else
            {
                page = 0;
            }
            const string query = "Select e.Id, e.FirstName, e.SecondName, e.Surname, e.SecondSurname, e.Identification, e.IdentificationTypeId, e.Email, e.AdmissionDate, e.RegistrationDate, e.AreaId, e.State, e.CountryId, c.Id, c.Name, c.Domain, a.Id, a.Name, it.Id, it.Name from Employees e inner join Countries c on e.CountryId = c.Id inner join Areas a on a.Id = e.AreaId inner join IdentificationTypes it on it.Id = e.IdentificationTypeId order by e.Id OFFSET @Page Rows FETCH NEXT @ItemsPerPage ROWS ONLY";
            using (var db = _dbFactory.GetConnection())
            {
                return db.Query(query, MapEmployee(), new {Page = page, ItemsPerPage = itemsPerPage});
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EmployeeModel GetById(int id)
        {
            const string query = "Select e.Id, e.FirstName, e.SecondName, e.Surname, e.SecondSurname, e.Identification, e.IdentificationTypeId, e.Email, e.AdmissionDate, e.RegistrationDate, e.AreaId, e.State, e.CountryId, c.Id, c.Name, c.Domain, a.Id, a.Name, it.Id, it.Name from Employees e inner join Countries c on e.CountryId = c.Id inner join Areas a on a.Id = e.AreaId inner join IdentificationTypes it on it.Id = e.IdentificationTypeId where e.Id = @Id";
            using (var db = _dbFactory.GetConnection())
            {
                return db.Query(query, MapEmployee(), new { Id = id }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Delete an employee
        /// </summary>
        /// <param name="employee"></param>
        /// <exception cref="ManagerException"></exception>
        public void Delete(EmployeeModel employee)
        {
            try
            {
                using (var db = _dbFactory.GetConnection())
                {
                    db.Delete(employee);
                }
            }
            catch (SqlException sqlException)
            {
                throw new ManagerException(ExceptionTypes.DataBase, sqlException.Errors[0].Message);
            }
            catch (Exception e)
            {
                throw new ManagerException(ExceptionTypes.DataBase, e.Message);
            }
        }

        /// <summary>
        /// Update employee
        /// </summary>
        /// <param name="employee"></param>
        public void UpdateEmployee(EmployeeModel employee)
        {
            if (employee == null)
            {
                throw new ArgumentNullException(nameof(employee));
            }
            
            using (var db = _dbFactory.GetConnection())
            {
                employee.EditionDate = DateTime.Now;
                if (!db.Update(employee))
                {
                    throw new ManagerException(ExceptionTypes.DataBase,
                        "El registro que intenta actualizar no existe en base de datos");
                }
            }
        }

        private Func<EmployeeModel, CountryModel, AreaModel, IdentificationTypeModel, EmployeeModel> MapEmployee()
        {
            return (employee, country, area, identification) =>
            {
                employee.Country = country;
                employee.Area = area;
                employee.IdentificationType = identification;
                return employee;
            };
        }
    }
}
