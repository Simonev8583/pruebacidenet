﻿using System.Data;
using System.Data.SqlClient;
using Cidenet.Domain.Interfaces.Generic;

namespace Cidenet.Infra.Data.Repository.Generic
{
    /// <summary>
    /// Db factory 
    /// </summary>
    public class DbFactory: IDbFactory

    {
        private readonly string _stringConnection;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connection"></param>
        public DbFactory(string connection)
        {
            _stringConnection = connection;
        }


        /// <summary>
        /// Get DB connection
        /// </summary>
        /// <returns></returns
        public IDbConnection GetConnection()
        {
            var connection = new SqlConnection(_stringConnection);
            connection.Open();
            return connection;
        }
    }
}
