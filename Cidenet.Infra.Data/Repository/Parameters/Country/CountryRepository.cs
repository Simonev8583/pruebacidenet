﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.Country;
using Cidenet.Domain.Interfaces.Generic;
using Cidenet.Domain.Interfaces.Parameters.Country;
using Dapper.Contrib.Extensions;

namespace Cidenet.Infra.Data.Repository.Parameters.Country
{
    /// <summary>
    /// Implement
    /// </summary>
    public class CountryRepository: ICountryRepository
    {
        private readonly IDbFactory _dbFactory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbFactory"></param>
        public CountryRepository(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CountryModel> GetAll()
        {
            using (var db = _dbFactory.GetConnection())
            {
                return db.GetAll<CountryModel>();
            }
        }
    }
}
