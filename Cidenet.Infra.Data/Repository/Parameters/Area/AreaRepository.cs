﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.Area;
using Cidenet.Domain.Interfaces.Generic;
using Cidenet.Domain.Interfaces.Parameters.Area;
using Dapper.Contrib.Extensions;

namespace Cidenet.Infra.Data.Repository.Parameters.Area
{
    /// <summary>
    /// Implement
    /// </summary>
    public class AreaRepository: IAreaRepository
    {
        private readonly IDbFactory _dbFactory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbFactory"></param>
        public AreaRepository(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AreaModel> GetAll()
        {
            using (var db = _dbFactory.GetConnection())
            {
                return db.GetAll<AreaModel>();
            }
        }
    }
}
