﻿using System.Collections.Generic;
using Cidenet.Domain.Entities.Parameters.IdentificationType;
using Cidenet.Domain.Interfaces.Generic;
using Cidenet.Domain.Interfaces.Parameters.IdentificationType;
using Dapper.Contrib.Extensions;

namespace Cidenet.Infra.Data.Repository.Parameters.IdentificationType
{
    /// <summary>
    /// Implement
    /// </summary>
    public class IdentificationTypeRepository: IIdentificationTypeRepository
    {
        private readonly IDbFactory _dbFactory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbFactory"></param>
        public IdentificationTypeRepository(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        /// <summary>
        /// All
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IdentificationTypeModel> GetAll()
        {
            using (var db = _dbFactory.GetConnection())
            {
                return db.GetAll<IdentificationTypeModel>();
            }
        }
    }
}
